#!/bin/bash

TEST_DIR="/TEST_DIR"

echo "Proj folders are here /TEST_DIR"

##create project dir
sudo mkdir -p $TEST_DIR/Proj1 $TEST_DIR/Proj2 $TEST_DIR/Proj3


##create groups
sudo groupadd developers -g 10001
sudo groupadd imanager -g 10010
sudo groupadd analytic -g 10100

##create users
##developers
sudo useradd -g 10001 R1
sudo useradd -g 10001 R2
sudo useradd -g 10001 R3
sudo useradd -g 10001 R4
sudo useradd -g 10001 R5
##info_manager
sudo useradd -g 10010 I1
sudo useradd -g 10010 I2
sudo useradd -g 10010 I3
##analytic
sudo useradd -g 10100 A1
sudo useradd -g 10100 A2
sudo useradd -g 10100 A3
sudo useradd -g 10100 A4

##set ACL for projects
##default rules
sudo setfacl -dm u:R2:rwX,u:R3:rwX,u:R5:rwX,u:A1:rwX,u:A4:r-X,g:imanager:rwX,u::rwX,g::-,o:-   $TEST_DIR/Proj1
sudo setfacl -dm u:R1:rwX,u:R5:rwX,u:A1:rwX,u:A2:r-X,u:A3:r-X,g:imanager:rwX,u::rwX,g::-,o:-  $TEST_DIR/Proj2
sudo setfacl -dm u:R1:rwX,u:R2:rwX,u:R4:rwX,u:A2:rwX,u:A1:r-X,u:A4:r-X,g:imanager:rwX,u::rwX,g::-,o:-   $TEST_DIR/Proj3
##rules
sudo setfacl -m u:R2:rwX,u:R3:rwX,u:R5:rwX,u:A1:rwX,u:A4:r-X,g:imanager:rwX,u::rwX,g::-,o:-   $TEST_DIR/Proj1
sudo setfacl -m u:R1:rwX,u:R5:rwX,u:A1:rwX,u:A2:r-X,u:A3:r-X,g:imanager:rwX,u::rwX,g::-,o:-  $TEST_DIR/Proj2
sudo setfacl -m u:R1:rwX,u:R2:rwX,u:R4:rwX,u:A2:rwX,u:A1:r-X,u:A4:r-X,g:imanager:rwX,u::rwX,g::-,o:-   $TEST_DIR/Proj3
